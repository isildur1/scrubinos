ARG PYTHON_VERSION

FROM python:${PYTHON_VERSION}

COPY . /scrubinos

ENV PATH $PATH:/scrubinos

WORKDIR /scrubinos

RUN python3 -m pip install --no-cache-dir -r requirements.txt --user

ENTRYPOINT ["python3"]

CMD ["--version"]
