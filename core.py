# -*- coding: utf-8 -*-
import random
import yaml

from datetime import datetime
from helpers import (
    load_conf,
    get_next_weekday,
    get_2_next_weekday,
    get_3_pairs,
    set_pair_task,
)


def run():
    copinos = load_conf("copinos.yaml")
    tasks = load_conf("tasks.yaml")
    now = datetime.now()
    nextw = get_next_weekday(now, 2)
    next4w = get_2_next_weekday(nextw, 2)
    pairs = get_3_pairs(list(copinos))
    tasks_list = list(tasks)
    random.shuffle(tasks_list)
    pairs_tasks = set_pair_task(pairs, tasks_list)

    planning = {}

    for w in next4w:
        readable_w = "{}-{}-{}".format(w.year, w.month, w.day)
        planning[readable_w] = pairs_tasks
        tasks_list = tasks_list[-1:] + tasks_list[:-1]
        pairs_tasks = set_pair_task(pairs, tasks_list)

    for date in list(planning):
        print(date)
        print(planning[date])

    with open("planning.yaml", "w") as file:
        yaml.dump(planning, file)


if __name__ == '__main__':
    run()
