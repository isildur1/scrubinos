import argparse
import sys
import codecs

from scrubinos import NAME, VERSION
from scrubinos.core import Scrubpairs


def get_args_parser():
    p = argparse.ArgumentParser(prog=NAME, description="Keep calm and scrub.")
    p.add_argument("--version", action="version", version="%(prog)s {}".format(VERSION))
    p.add_argument(
        "--copinos-conf",
        "-conf",
        default="copinos.yaml",
        help="Provide copinos mapping between names and emails in yaml format",
    )
    p.add_argument(
        "--tasks-conf",
        "-tasks",
        default="tasks.yaml",
        help="Provide tasks mapping between names and desc in yaml format",
    )
    return p


def run(args=None):
    # Wrap `sys.stdout` in an object that automatically encodes an unicode
    # string into utf-8, in Python 2 only. The default encoding for Python 3
    # is already utf-8.
    if sys.version_info[0] < 3:
        sys.stdout = codecs.getwriter("utf-8")(sys.stdout)
    options = get_args_parser().parse_args(args if args is not None else sys.argv[1:])
    scrubpairs = Scrubpairs(options.__dict__)
    try:
        scrubpairs.run()
    except KeyboardInterrupt:
        print("user interupted")
