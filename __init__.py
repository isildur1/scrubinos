"""A utility to generate copinos scrub agenda
"""

NAME = "scrubinos"
VERSION = "0.0.1"
DESCRIPTION = __doc__

__author__ = "Yann Bouzonie"
__license__ = "MIT"
__version__ = VERSION
