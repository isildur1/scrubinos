# Alerting bot to remind copinos scrub planning

![logo](goodhousekeeping.jpg)

## Run it baby !

### Prequisites

#### CONF

- Fill in the [template.copinos.yaml](./template.copinos.yaml)
- Rename it to ```copinos.yaml```
- Fill in the [template.tasks.yaml](./template.tasks.yaml)
- Rename it to ```tasks.yaml```

#### GMAIL
- Follow [Google instructions](https://developers.google.com/identity/protocols/OAuth2) in order to use Oauth2 lib to send emails
- Some other hint [here](https://developers.google.com/gmail/api/quickstart/python)
- Fill in the [default.env.py](./default.env.py)
- Rename it to ```env.py```

### Go

- Send emails
```bash
python3 alert.py
```

- Generate a random planning
```
python3 core.py
```
