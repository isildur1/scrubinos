import core
import emailtemp

from emailtemp import FROM, SUBJECT, BODY

from datetime import datetime

from helpers import load_conf, get_partner, get_paired_task, get_next_weekday

from oauth2 import send_mail


def alert(dateplan: dict, copinos: dict, tasks: dict):
    """
    email params specs:
        1 - binomial name
        2 - task name
        3 - task desc
    """
    for map in dateplan:
        partners = list(map[0])
        for colloc in partners:
            partner = get_partner(partners, colloc).upper()
            task_name = get_paired_task(dateplan, colloc)
            body = BODY.format(partner, task_name)
            body += getattr(emailtemp, tasks[task_name]["desc"])
            send_mail(
                FROM,
                "{}".format(copinos[colloc]["email"]),
                SUBJECT,
                body,
                "./images/{}".format(tasks[task_name]["img"]),
            )


planning = load_conf("planning.yaml")
copinos = load_conf("copinos.yaml")
tasks = load_conf("tasks.yaml")


now = datetime.now()
raw_nextw = get_next_weekday(now, 2)
nextw = "{}-{}-{}".format(raw_nextw.year, raw_nextw.month, raw_nextw.day)


if nextw not in list(planning):
    core.run()
    planning = load_conf("planning.yaml")

alert(planning[nextw], copinos, tasks)
