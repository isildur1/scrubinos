import calendar
import random
import itertools
import yaml

from datetime import datetime
from datetime import timedelta
from pathlib import Path


def get_first_mondays(year):
    mondays = {}
    for month in range(1, 13):
        cal = calendar.monthcalendar(year, month)
        week1 = cal[0]
        week2 = cal[1]
        if week1[calendar.MONDAY] != 0:
            genday = week1[calendar.MONDAY]
        else:
            genday = week2[calendar.MONDAY]
        mondays[month] = genday
    return mondays


def get_next_weekday(start: datetime, weekday):
    """
    Return the next desired weekday
    Keyword arguments:
    start -- the day from where you start calucaltion (in datetime format)
    weekday -- 0 = Monday, 1 = Tuesday, ...
    """
    days_ahead = weekday - start.weekday()
    if days_ahead <= 0:  # target day already happened this week
        days_ahead += 7
    return start + timedelta(days_ahead)


def get_2_next_weekday(start: datetime, weekday):
    res = [start]
    for _ in range(0, 2):
        res.append(get_next_weekday(res[-1], weekday))
    return res


def get_random_pairs(collocs: list):
    # Generate all possible non-repeating pairs
    pairs = list(itertools.combinations(collocs, 2))
    # Randomly shuffle these pairs
    random.shuffle(pairs)
    return pairs


def get_3_pairs(collocs: list):
    pairs = [get_random_pairs(collocs)[0]]
    for _ in range(0, 2):
        collocs.remove(pairs[-1][0])
        collocs.remove(pairs[-1][1])
        pairs.append(get_random_pairs(collocs)[0])
    return pairs


def load_conf(conf: Path):
    if Path(conf).exists():
        yaml_file = open(conf)

    else:
        print("Not found" + str(conf) + " will create an empty one")
        f = open("{}".format(str(conf)), "w+")
        f.close
        yaml_file = open(conf)
    return yaml.load(yaml_file, Loader=yaml.FullLoader)


def get_partner(binomial, colloc):
    if colloc == binomial[0]:
        partner = binomial[1]
    else:
        partner = binomial[0]
    return partner


def set_pair_task(pairs, tasks_seq):
    pairs_tasks = [[pairs[0], tasks_seq[0]]]
    for i in range(1, 3):
        pairs_tasks.append([pairs[i], tasks_seq[i]])
    return pairs_tasks


def get_paired_task(pairs_tasks, colloc):
    for pt in pairs_tasks:
        if colloc in pt[0]:
            return pt[1]
